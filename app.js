const express = require("express");
const basicAuth = require('express-basic-auth');
// Get constant files. my-courses.json will be fetched in the functions since it is dynamic.
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
const cors = require('cors');
mongoose.connect("mongodb+srv://dbUser1:password42@cluster0.bcfiq.mongodb.net/CarShare?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    console.log("Connected to db");
});

// Write some mongoose models to interact with the collections

var Car = require("./models/cars.js");

var Customer = require("./models/customers.js");

var Area = require("./models/areas.js");

// Create an express instance
const app = express();

app.use(bodyParser.json(), cors());

var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Connection port
const port = process.env.PORT || 3000; // https://stackoverflow.com/a/18864718

// Return all customers
app.get('/api/customers', function(req, res) {
    Customer.find({}, {
        '_id': 0 // Exclude id field
    }, function(err, customers) {
        if (err) {
            res.status(500).send(err);
        }
        res.status(200).json(customers);
    })
});

// Update membership
app.put('/api/customers/update-membership/:id', function(req, res) {
    Customer.findOne({ id: req.params.id }).then((result) => {
        if (result.Membership) { // Switch the value for membership true -> false, false -> true
            Customer.updateOne({ id: req.params.id }, { Membership: false }).then(() => {});
        } else {
            Customer.updateOne({ id: req.params.id }, { Membership: true }).then(() => {});
        }
        res.status(200).json({ message: "Updated membership" });
    })
});

// Customer update area, if the customer is renting a car, update the area of the car
app.put('/api/customers/update-area', function(req, res) {
    Customer.findOne({ id: req.body.id }).then((result) => {
        if (result !== null) {
            if (result.Car !== "") { // Check if the Customer has a car and if so update the area of car.
                Car.updateOne({ Registration_number: result.Car }, { Area: req.body.Area }).then(() => {})
            }
            Customer.updateOne({ id: req.body.id }, { Area: req.body.Area }).then(() => {
                res.status(200).json({ message: "Area updated" });
            });
        }
    })
});

// If the customer is currently hiring a car, then a customer shouldn't be able to rent another car
app.put('/api/customers/rent-car', function(req, res) {
    Customer.findOne({ id: req.body.id }).then((result) => { // Check that the customer exists
        if (result !== null) {
            if (result.Car === "") { // Check to see that the customer isn't currently renting a car
                Customer.updateOne({ id: req.body.id }, { Car: req.body.Car }).then(() => { // Update the Car field
                    res.status(200).json({ message: "The customer is renting car" + req.body.Car });
                });
            } else {
                res.status(409).json({ message: "The car the customer is currenty renting must be dropped before renting a new car" })
            }
        } else {
            res.status(500).json({ message: "The customer doesn't exist" });
        }
    })
});

// If customer with id has a car set it to ""
app.put('/api/customers/drop-car/:id', function(req, res) {
    Customer.findOne({ id: req.params.id }).then((result) => {
        if (result.Car !== "") {
            Car.findOne({ Registration_number: result.Car }).then((result) => { // Update mileage by a random number between 1-100
                console.log(result)
                var newMileage = (parseInt(result.Mileage) + parseInt(1 + Math.floor(Math.random() * 100))); // When the customer drops a car the mileage is updated by a random number 1-100 
                console.log(newMileage);
                Car.updateOne({ _id: result._id }, { Mileage: newMileage }).then((result) => {
                    console.log(result);
                    console.log("Updated mileage " + newMileage);
                })
            })
        }
        Customer.updateOne({ id: req.params.id }, { Car: "" }).then(() => { // Update the Car field to empty
            res.status(200).json({ message: "The customer dropped the car" });
        });
    });
});

// Add customer
app.post('/api/customers/add', urlencodedParser, (req, res) => {
    // Check if there already exists a customer with that id before adding the customer
    Customer.countDocuments({ id: req.body.id }).then((result) => {
        if (result == 0) {
            var newCustomer = new Customer({ // create new instance of Customer
                Name: req.body.name,
                Address: req.body.address,
                Car: req.body.car,
                Age: req.body.age,
                Membership: req.body.membership,
                Area: req.body.area,
                id: req.body.id
            });
            newCustomer.save(function(err) { // Save that instance to the collection
                if (err) return res.status(500);
            });
            res.status(201).json({ message: "customer added" });
        } else {
            res.status(409).json({ message: "customer with that id already exists" });
        }
    });
});

// Deletes a customer based on id, checks if the customer is currently renting by checking that the car field = ""
app.delete('/api/customers/delete/:id', function(req, res) {
    let pId = parseInt(req.params.id); // parse to int as id field is of type int
    console.log(pId);
    Customer.deleteOne({ id: pId, Car: "" }). // Delete customer only if the customer is not currently renting
    then(result => {
        if (result.deletedCount == 1) { // Check if a document was deleted, if not send error
            res.status(200).json({ message: 'Deleted customer with id ' + pId })
        } else {
            res.status(400).json({ message: 'Customer could not be deleted as the it is currently renting a vehicle' })
        }
    }, reason => {
        res.status(400).json(reason);
    });
});

// Return all cars including the id and name of the customer renting the car.
app.get('/api/cars', function(req, res) {
    Car.aggregate([{
        '$lookup': { // For each document in cars, return the customer currently renting a car
            'from': 'customers',
            'localField': 'Registration_number',
            'foreignField': 'Car',
            'as': 'Driver'
        }
    }, {
        '$unwind': { // Then deconstruct the array and..
            'path': '$Driver',
            'preserveNullAndEmptyArrays': true
        }
    }, {
        '$addFields': { // .. add the required fields
            'Driver': '$Driver.Name',
            'Driver_id': '$Driver.id'
        }
    }]).then(cars => {
        res.json(cars);
    })
});

// Get one car
app.get('/api/cars/:id', function(req, res) {
    Car.aggregate([{
        '$match': { // Same aggregation pipeline as above apart from the match
            'Registration_number': req.params.id
        }
    }, {
        '$lookup': {
            'from': 'customers',
            'localField': 'Registration_number',
            'foreignField': 'Car',
            'as': 'Driver'
        }
    }, {
        '$unwind': {
            'path': '$Driver',
            'preserveNullAndEmptyArrays': true
        }
    }, {
        '$addFields': {
            'Driver': '$Driver.Name',
            'Driver_id': '$Driver.id'
        }
    }]).then(cars => {
        res.json(cars);
    })
})

// Add car
app.post('/api/cars/add', urlencodedParser, (req, res) => {
    // Check if there already exists a car with that reg-number before adding
    Car.countDocuments({ Registration_number: req.body.regNum }).then((result) => {
        if (result == 0) {
            var newCar = new Car({ // Create instance of Car model
                Registration_number: req.body.regNum,
                Make: req.body.make,
                Model: req.body.model,
                Year: req.body.year,
                Seats: req.body.seats,
                Rate: req.body.rate,
                Fuel: req.body.fuel,
                Mileage: req.body.mileage,
                Area: req.body.area
            });
            newCar.save(function(err) { // save it to collection
                if (err) return res.status(500);
            });
            res.status(201).json({ message: "car added" });
        } else {
            res.status(409).json({ message: "car with registration number " + req.body.Registration_number + " already exists" });
        }
    });
})

// Delete car
app.delete('/api/cars/delete/:id', function(req, res) {
    Customer.countDocuments({ Car: req.params.id }).then((result) => { // Ensure the car is not currently in use before deletion
        if (result == 0) {
            Car.deleteOne({ Registration_number: req.params.id }, function(error, result) {
                if (result.deletedCount === 1) { // Check if a car was deleted
                    res.status(200).json({ message: "deleted car " + req.params.id });
                } else {
                    res.status(409).json({ message: "car with registration number " + req.params.id + " doesn't exist" });
                }
                if (error) {
                    res.status(500).json(error);
                }
            });
        } else {
            res.status(409).json({ message: "car with registration number " + req.params.id + " is in use and so could therefore not be deleted" });
        }
    });
});

// Update the area of the car if it is not in use
app.put('/api/cars/update/area', function(req, res) {
    Customer.countDocuments({ Car: req.body.Registration_number }).then((result) => { // Check if the car is currently in use
        if (result === 1) {
            res.status(409).json({ message: "Can not relocate car whilst car is in use" });
        } else {
            Car.updateOne({ Registration_number: req.body.Registration_number }, { Area: req.body.Area }).then(() => { // If not update the area.
                res.status(200).json({ message: "The area for the car has been updated" });
            })
        }

    });
});

// Returns the areas including the number of customers 
// and cars aggregated from cars and customers
app.get('/api/areas', function(req, res) {
    Area.aggregate([{
        '$lookup': {
            'from': 'customers',
            'localField': 'Area',
            'foreignField': 'Area',
            'as': 'customers_array'
        }
    }, {
        '$addFields': {
            'Nr_customers': {
                '$size': '$customers_array' // count number of elements in the array
            }
        }
    }, {
        '$lookup': {
            'from': 'cars',
            'localField': 'Area',
            'foreignField': 'Area',
            'as': 'cars_array'
        }
    }, {
        '$addFields': {
            'Nr_cars': {
                '$size': '$cars_array' // count number of elements in the array
            }
        }
    }, {
        '$project': { // Remove unwanted fields
            '_id': 0,
            'customers_array': 0,
            'cars_array': 0
        }
    }]).then(areas => {
        res.json(areas);
    });
});

// Add area 
app.post('/api/areas/add', urlencodedParser, (req, res) => {
    newArea = new Area(req.body);
    Area.countDocuments({ Area: req.body.Area }).then((result) => {
        if (result == 0) {
            newArea.save().then(() => {
                res.status(201).json({ message: "Added area " + req.body.Area })
            });
        } else {
            res.status(409).json({ message: "Area " + req.body.Area + " already exists" });
        }
    });
});

// Delete area
app.delete('/api/areas/delete/:id', function(req, res) {
    // When deleting an area we want to move the cars and customers to another area
    if (req.params.id == "Luthagen") { // Make sure there is at least one remaining area to reallocate customers.
        res.status(500).json({ message: "Can not delete area of service hub" })
    } else {
        // Check if there are any customers using the service in the area to be deleted, if so send error.
        Customer.countDocuments({ Car: { $ne: "" }, Area: req.params.id }).then(result => {
            if (result !== 0) {
                res.status(409).json({ message: "Can not delete area whilst a customer is using the service in the area" })
            } else {
                Customer.updateMany({ Area: req.params.id }, { $set: { Area: "Luthagen" } }).then(() => { // Moves customers to luthagen 
                })
                Area.aggregate([{ // Find the area with the least number of cars, it will be to this area the cars will be redeployed to
                    '$lookup': {
                        'from': 'cars',
                        'localField': 'Area',
                        'foreignField': 'Area',
                        'as': 'cars_array'
                    }
                }, {
                    '$group': {
                        '_id': '$Area',
                        'Nr_cars': {
                            '$max': {
                                '$size': '$cars_array'
                            }
                        }
                    }
                }, {
                    '$sort': {
                        'Nr_cars': 1
                    }
                }, {
                    '$limit': 1
                }, {
                    '$project': {
                        '_id': 1
                    }
                }]).then((result) => {
                    Car.updateMany({ Area: req.params.id }, { $set: { Area: result[0]._id } }).then((result) => { // Moves customers to Luthagen 
                        console.log(result);
                        Area.deleteOne({ Area: req.params.id }).then(() => {
                            res.status(200).json({ message: "Deleted area: " + req.params.id });
                        })
                    })
                })
            }
        });
    }
});


app.listen(port, function() {
    console.log("Server running on port " + port);
});