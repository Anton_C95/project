# Uppsala car share app

## Summary

### Data 

The app will consist of three collections, Customers, Cars and Areas. 

![](Uppsala_car_share.png)

Initially there will be 35 customers, 10 cars and 5 areas, 50 documents in total. 

## Functionality

The information for each collection will be displayed in tables in their respective page. When an adminstrator has logged in the CRUD functionality will be enabled for each page.  

### customer.html

If the area of a customer is updated whilst in use of a car, the car document should also be updated. 

When hovering over the area for a customer, the number of cars in the area should show.

Car and Area will have update functionality.

A drop down list of available cars in the customers' area will be used to update the car for the customer.

The rate will be displayed if the customer is using a car, the rate will depend on whether the customer is a member or not. 

A customer can only be deleted if a car is not in use, i.e "Car": null. 


### Customers document example
``` 
{
        "Name": "Karin Svensson",
        "Address": "Djäknegatan 89",
        "Car": "RHS-129",
        "Age": 34,
        "Membership": true,
        "Area": "Kvarngärdet"
}
```

### area.html

Displays the number of customers in the area as well as the number of cars (information retrieved from Cars and Customers collection).

Population will have update functionality. 

An area can only be deleted if no customers are in the area. Cars located in the area will be relocated to the area with the highest number of customers. 

### Area document example

```
{
        "Area": "Kvarngärdet",
        "Population": 15000,
        "Size": 40
}
```



### car.html

Mileage will be incremented by a random number between 0-100 when a customer drops the car. 

The rates for the car can be updated.

A car can only be deleted if no customer is using the car.

### Customer document example

```
{
        "Registration_number": "OLK-122",
        "Make": "Volvo",
        "Model": "V60",
        "Year": 2012,
        "Seats": 5,
        "Rate": [90, 110],
        "Fuel": "Petrol",
        "Mileage": 2000
}
```


## Rentals.
When a customer drops a vehicle a post could be made to rentals, adding
a document with the carreg, customer, duration (random number?), and price. This could be published in a separate page. 