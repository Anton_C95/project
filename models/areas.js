var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var areasSchema = new Schema({
    Area: String,
    Population: Number,
    Size: Number
});

module.exports = mongoose.model("area", areasSchema);