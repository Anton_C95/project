var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var carsSchema = new Schema({
    Registration_number: String,
    Make: String,
    Model: String,
    Year: Number,
    Seats: Number,
    Rate: Array,
    Fuel: String,
    Mileage: Number,
    Area: String
});

module.exports = mongoose.model("car", carsSchema);