var mongoose = require("mongoose");
var Schema = mongoose.Schema;


var customersSchema = new Schema({
    Name: String,
    Address: String,
    Car: String,
    Age: Number,
    Membership: Boolean,
    Area: String,
    id: Number
});


module.exports = mongoose.model("customer", customersSchema);